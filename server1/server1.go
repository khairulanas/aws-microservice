package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func returnresponse(w http.ResponseWriter, r *http.Request) {
	url := "https://api.rajaongkir.com/starter/province?id=12"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("key", "6d2fdceb4dadc7f503ef4a023bace055")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	var responseObj RajaOngkir
	json.Unmarshal(body, &responseObj)

	json.NewEncoder(w).Encode(responseObj)
}

func main() {
	http.HandleFunc("/", returnresponse)
	log.Fatal(http.ListenAndServe(":1234", nil))

}

type RajaOngkir struct {
	Rajaongkir struct {
		Query struct {
			ID string `json:"id"`
		} `json:"query"`
		Status struct {
			Code        int    `json:"code"`
			Description string `json:"description"`
		} `json:"status"`
		Results struct {
			ProvinceID string `json:"province_id"`
			Province   string `json:"province"`
		} `json:"results"`
	} `json:"rajaongkir"`
}
