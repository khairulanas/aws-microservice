package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func returnRajaongkir(w http.ResponseWriter, r *http.Request) {
	response, err := http.Get("http://18.215.148.55:1234/")

	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var responseObj RajaOngkir
	json.Unmarshal(responseData, &responseObj)

	json.NewEncoder(w).Encode(responseObj)
}
func returnIndoNews(w http.ResponseWriter, r *http.Request) {
	response, err := http.Get("http://localhost:4321/")

	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var responseObj IndoNews
	json.Unmarshal(responseData, &responseObj)

	json.NewEncoder(w).Encode(responseObj)
}
func main() {
	http.HandleFunc("/rajaongkir", returnRajaongkir)
	http.HandleFunc("/indonews", returnIndoNews)
	fmt.Print("run servergate port : 5000")
	log.Fatal(http.ListenAndServe(":5000", nil))

}

type IndoNews struct {
	Status       string `json:"status"`
	TotalResults int    `json:"totalResults"`
	Articles     []struct {
		Source struct {
			ID   interface{} `json:"id"`
			Name string      `json:"name"`
		} `json:"source"`
		Author      string    `json:"author"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		URL         string    `json:"url"`
		URLToImage  string    `json:"urlToImage"`
		PublishedAt time.Time `json:"publishedAt"`
		Content     string    `json:"content"`
	} `json:"articles"`
}

type RajaOngkir struct {
	Rajaongkir struct {
		Query struct {
			ID string `json:"id"`
		} `json:"query"`
		Status struct {
			Code        int    `json:"code"`
			Description string `json:"description"`
		} `json:"status"`
		Results struct {
			ProvinceID string `json:"province_id"`
			Province   string `json:"province"`
		} `json:"results"`
	} `json:"rajaongkir"`
}
