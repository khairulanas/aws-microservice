package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func returnresponse(w http.ResponseWriter, r *http.Request) {
	url := "http://newsapi.org/v2/top-headlines?country=id&apiKey=4281777e7eb140aa971682312d82e0b9"

	req, _ := http.NewRequest("GET", url, nil)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	var responseObj IndoNews
	json.Unmarshal(body, &responseObj)

	json.NewEncoder(w).Encode(responseObj)
}

func main() {
	http.HandleFunc("/", returnresponse)
	log.Fatal(http.ListenAndServe(":4321", nil))

}

type IndoNews struct {
	Status       string `json:"status"`
	TotalResults int    `json:"totalResults"`
	Articles     []struct {
		Source struct {
			ID   interface{} `json:"id"`
			Name string      `json:"name"`
		} `json:"source"`
		Author      string    `json:"author"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		URL         string    `json:"url"`
		URLToImage  string    `json:"urlToImage"`
		PublishedAt time.Time `json:"publishedAt"`
		Content     string    `json:"content"`
	} `json:"articles"`
}
